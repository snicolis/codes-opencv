## code to track one indivicual

#%%
from CommonModules import  *
import cv2
import cv2 as cv
import scipy.ndimage as ndimage
from scipy.signal import savgol_filter
import numba
from numba import jit
import copy
from scipy.optimize import curve_fit


font = cv2.FONT_HERSHEY_COMPLEX 
#%%
def run_analysis(file):
        video = file
        f   = open(video+"_blob.txt","w+")
        f2  = open(video+"_cadeconne.txt", "w+")
        cap = cv.VideoCapture(video+'.mp4')
        ret, frame = cap.read()
        im = []
        for i in range(50):
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                im.append(frame)
                ret, frame = cap.read()
        mean1 = np.median(im)
        cap    = cv.VideoCapture(video+'.mp4')
        length = int(cap.get(cv.CAP_PROP_FRAME_COUNT))

        print('\nnumber of frames =', length)

        cumul = np.zeros((720, 1280))
        ret, current_frame = cap.read()
        ii = 0
        xc1a = 270
        xc2a = 645  
        xc3a = 270
        xc4a = 645
        xc1b = xc1a + 330
        xc2b = xc2a + 330
        xc3b = xc3a + 330  
        xc4b = xc4a + 330



        yc1a = 0
        yc2a = 0
        yc3a = 373
        yc4a = 378
        yc1b = yc1a + 330
        yc2b = yc2a + 330
        yc3b = yc3a + 330
        yc4b = yc4a + 330


        xxc1 = [xc1a, xc2a, xc3a, xc4a]
        yyc1 = [yc1a, yc2a, yc3a, yc4a]
        xxc2 = [xc1b, xc2b, xc3b, xc4b]
        yyc2 = [yc1b, yc2b, yc3b, yc4b]

        kernel2 = np.ones((10,10),np.uint8)
        jj = 0
        while(cap.isOpened()):
                ii = ii + 1
        
                if ii > 2:
                        jj = jj+1
                        current_frame_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
                        current_frame_gray1 = cv.absdiff(current_frame_gray, mean1)
                        current_frame_gray1 = cv2.dilate(current_frame_gray1,kernel2,iterations = 1)
                        
                        for iq in range(4):
                                ind = iq
                                cimg =  current_frame[yyc1[ind]:yyc2[ind], xxc1[ind]:xxc2[ind]]
                                im1 = current_frame_gray1[yyc1[ind]:yyc2[ind], xxc1[ind]:xxc2[ind]]
                                ret,thresh1 = cv.threshold(im1,127,255,cv.THRESH_BINARY)
                                # thresh1 = cv2.dilate(thresh1, kernel2)
                                contours, hierarchy = cv.findContours(thresh1, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
                                if len(contours) == 0:
                                        x = np.nan
                                        y = np.nan
                                        print(ii, '\t', iq+1, file=f2)
                                        print (ii,'\t', iq+1, '\t', 1, '\t',x, '\t',y, file=f)
                                        # print(ii, '\t', iq+1)
                                params = cv2.SimpleBlobDetector_Params()
                                j = 0
                                for c in contours:
                                        j = j + 1
                                        x,y,w,h = cv2.boundingRect(c)
                                        if cv2.contourArea(c) < 5 or x < 5 or y < 5 or cv2.contourArea(c) > 1000:
                                                x = np.nan
                                                y = np.nan
                                                print(ii, '\t', iq+1, file=f2)
                                                # print(ii, '\t', iq+1)
                                        else:       
                                                x,y,w,h = cv2.boundingRect(c)
                                                if ii == 100:
                                                        cv2.rectangle(cimg, (x, y), (x + w, y + h), (0, 255,0), 2)
                                        print (ii, '\t', iq+1, '\t', j, '\t',x, '\t',y, file=f)
                                        if np.mod(ii, 3600) == 0:
                                                print (ii, '\t', iq+1, '\t', j, '\t',x, '\t',y)
                                # cv2.drawContours(cimg, contours, -1, (0,255,0), 3)                
                                # cv2.wrire("frame_"+str(iq), cimg)

                                if ii <= 3600:
                                        cv2.drawContours(cimg, contours, -1, (0,255,0), 3)
                                
                                        cv2.imwrite("movie/"+"frame_"+str(iq)+"_"+str(ii).zfill(6)+".png", cimg)

                        ret, current_frame = cap.read()
                key = cv.waitKey(1)
                if key == 27 or ii== 3600:  
                        break
        return ind


pp = ['Menthe4bp18']
for i in range(1):
        print(pp[i])
        run_analysis(pp[i])

